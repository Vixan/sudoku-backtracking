#pragma once

#include <iostream>
#include <vector>

#define BOARD_SIZE       9
#define BOARD_CELL_SIZE  3
#define RANDOM_FILL      6
#define EMPTY_CELL       0
#define INITIAL_POSITION 0

struct Board {
private:
	int grid[BOARD_SIZE][BOARD_SIZE];
	int row_current;
	int col_current;
public:
	Board();
	Board(int grid[BOARD_SIZE][BOARD_SIZE]);
	Board(int rand_fill);
	~Board();

	static bool is_valid_grid(int grid[BOARD_SIZE][BOARD_SIZE]);
	static void fill_empty(int grid[BOARD_SIZE][BOARD_SIZE]);
	bool has_empty_cells();
	void print_grid();
	bool solve();
	bool is_valid(int number);
};