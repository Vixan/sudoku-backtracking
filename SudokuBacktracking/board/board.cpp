#include "board.hpp"

Board::Board() {
	int rand_indexes[RANDOM_FILL];

	Board::fill_empty(this->grid);
	this->row_current = INITIAL_POSITION;
	this->col_current = INITIAL_POSITION;

	for (int i = 0; i < RANDOM_FILL; i++) {
		int index = rand() % (BOARD_SIZE * BOARD_SIZE);
		rand_indexes[i] = index;
	}

	for (int i = 0; i < RANDOM_FILL; i++) {
		int index = rand_indexes[i];
		this->grid[index / BOARD_SIZE][index % BOARD_SIZE] = rand() % 9 + 1;
	}
}

Board::Board(int grid[BOARD_SIZE][BOARD_SIZE]) {
	Board::fill_empty(this->grid);
	this->row_current = INITIAL_POSITION;
	this->col_current = INITIAL_POSITION;

	for (int i = 0; i < BOARD_SIZE; i++) {
		for (int j = 0; j < BOARD_SIZE; j++) {
			this->grid[i][j] = grid[i][j];
		}
	}
}

Board::Board(int rand_fill) {
	std::vector<int> rand_indexes(rand_fill, EMPTY_CELL);

	Board::fill_empty(this->grid);
	this->row_current = INITIAL_POSITION;
	this->col_current = INITIAL_POSITION;

	for (int i = 0; i < rand_fill; i++) {
		int index = rand() % (BOARD_SIZE * BOARD_SIZE);
		rand_indexes[i] = index;
	}

	for (int i = 0; i < rand_fill; i++) {
		int index = rand_indexes[i];
		this->grid[index / BOARD_SIZE][index % BOARD_SIZE] = rand() % 9 + 1;
	}
}

Board::~Board() {
	std::cout << "\nBye, have a nice day!\n";
}

bool Board::is_valid_grid(int grid[BOARD_SIZE][BOARD_SIZE]) {
	for (int i = 0; i < BOARD_SIZE; i++) {
		for (int j = 0; j < BOARD_SIZE; j++) {
			if (grid[i][j] < 1 || grid[i][j] > 9) {
				return false;
			}
		}
	}

	return false;
}

void Board::fill_empty(int grid[BOARD_SIZE][BOARD_SIZE]) {
	for (int i = 0; i < BOARD_SIZE; i++) {
		for (int j = 0; j < BOARD_SIZE; j++) {
			grid[i][j] = EMPTY_CELL;
		}
	}
}

void Board::print_grid() {
	std::cout << "\n";

	for (int i = 0; i < BOARD_SIZE; i++) {
		for (int j = 0; j < BOARD_SIZE; j++) {
			std::cout << this->grid[i][j] << " ";
		}

		std::cout << "\n";
	}
}

bool Board::has_empty_cells() {
	for (this->row_current = 0; this->row_current < BOARD_SIZE; this->row_current++) {
		for (this->col_current = 0; this->col_current < BOARD_SIZE; this->col_current++) {
			if (this->grid[this->row_current][this->col_current] == EMPTY_CELL) {
				return true;
			}
		}
	}

	return false;
}

bool Board::solve() {
	if (!this->has_empty_cells()) {
		return true;
	}

	for (int n = 1; n <= BOARD_SIZE; n++) {
		if (this->is_valid(n)) {
			this->grid[this->row_current][this->col_current] = n;

			if (this->solve()) {
				return true;
			}

			this->grid[this->row_current][this->col_current] = EMPTY_CELL;

			if (this->col_current > 0) {
				this->col_current--;
			}
			else {
				this->col_current = 8;
				this->row_current--;
			}
		}
	}

	return false;
}

bool Board::is_valid(int number) {
	for (int i = 0; i < BOARD_SIZE; i++) {
		if (this->grid[i][this->col_current] == number) {
			return false;
		}

		if (this->grid[this->row_current][i] == number) {
			return false;
		}
	}

	int cell_row_index = this->row_current - this->row_current % 3;
	int cell_col_index = this->col_current - this->col_current % 3;

	for (int i = cell_row_index; i < cell_row_index + BOARD_CELL_SIZE; i++) {
		for (int j = cell_col_index; j < cell_col_index + BOARD_CELL_SIZE; j++) {
			if (this->grid[i][j] == number) {
				return false;
			}
		}
	}

	return true;
}
