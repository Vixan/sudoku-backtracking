#include <stdlib.h>
#include <time.h>

#include "main.hpp"

int main(int argc, char* argv[]) {
	srand((unsigned int)time(NULL));

	int grid[BOARD_SIZE][BOARD_SIZE] = {
	{ 3, 0, 6, 5, 0, 8, 4, 0, 0 },
	{ 5, 2, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 8, 7, 0, 0, 0, 0, 3, 1 },
	{ 0, 0, 3, 0, 1, 0, 0, 8, 0 },
	{ 9, 0, 0, 8, 6, 3, 0, 0, 5 },
	{ 0, 5, 0, 0, 9, 0, 6, 0, 0 },
	{ 1, 3, 0, 0, 0, 0, 2, 5, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 7, 4 },
	{ 0, 0, 5, 2, 0, 6, 3, 0, 0 }};

	Board board(grid);

	if (board.solve()) {
		board.print_grid();
	}
	else {
		std::cout << "Well...No solutions, I guess...\n";
		board.print_grid();
	}

	_getch();

	return 0;
}